function mean()
{
    let c = 0
    for(let i = 0; i < arguments.length; i ++)
    {
        c += arguments[i];
    }
    return c / arguments.length;
}

console.log(mean(1, 2, 3, 4, 5));