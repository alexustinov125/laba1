export default async function delay()
{
    return new Promise(resolve=>
        setTimeout(resolve, Math.random() * 1000)
    );
}
