const vowels = ["a", "e", "i", "o", "u", "y"]; 

function ask_password(login, password, success, failure)
{
    const login_sogl = login.
                            replaceAll('', ' ').
                            split(' ').filter(function(item)
                            {
                                return !vowels.includes(item) && isNaN(item)
                            });
    const pass_sogl = password.
                            replaceAll('', ' ').
                            split(' ').filter(function(item)
                            {
                                return !vowels.includes(item) && isNaN(item)
                            });
    const vowel_count = password.
                                replaceAll('', ' ').
                                split(' ').filter(function(item)
                                {
                                    return isNaN(item)
                                }).length - pass_sogl.length;

    if (vowel_count != 3 && pass_sogl.toString() != login_sogl.toString()) 
    {
        failure(login, "Everything is wrong");
    }
    else if (vowel_count != 3) 
    {
        failure(login, "Wrong number of vowels");
    }
    else if (pass_sogl.toString() != login_sogl.toString()) 
    {
        failure(login, "Wrong consonants");
    }
    else success(login);
}

function main(login, password)
{
    ask_password(login, password, function(log){console.log('Привет, ' + log + ' !')}, 
    function(log, msg)
    {
        console.log('Кто-то пытался притвориться пользователем ' + log + ', но в пароле допустил ошибку: ' + msg.toUpperCase());
    });
}


main("anastasia1337", "nsyatos22");
main("eugene", "aanig123123");