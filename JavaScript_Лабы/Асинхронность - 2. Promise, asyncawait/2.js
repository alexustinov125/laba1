async function readConfig (name) 
{
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log('(1) config from ' + name + ' loaded')
            resolve();
        }, Math.floor(Math.random() * 1000))
    });
}

async function doQuery (statement) 
{
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log('(2) SQL query executed: ' + statement)
            resolve();
        }, Math.floor(Math.random() * 1000))
    });
}

async function httpGet (url) 
{
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log('(3) Page retrieved: ' + url)
            resolve();
        }, Math.floor(Math.random() * 1000))
    });
}

async function readFile (path) 
{
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log('(4) Readme file from ' + path + ' loaded')
            resolve();
        }, Math.floor(Math.random() * 1000))
    });
}

readConfig('myConfig')
doQuery('select * from cities')
httpGet('http://google.com')
readFile('README.md')

//readConfig('myConfig').then(() => doQuery('select * from cities')).then(() => httpGet('http://google.com')).then(() => readFile('README.md'));