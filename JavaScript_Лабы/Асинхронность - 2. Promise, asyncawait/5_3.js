async function F(x)
{
    let res1 = await f1(x);
    let res2 = await f2(x, res1);
    let res3 = await f3(x, res2);
    let res4 = await f4(x, res3);
    let res5 = await f5(x, res4);
    let res6 = await f6(x, res5);
    console.log('F:', res6);
}

async function f1(x)
{
    let res = x ** 2;
    console.log('f1: ' + res);
    return res;      
}

async function f2(x, result)
{
    let res = result +  2 * x;
    console.log('f2: ' + res);
    return res;
}

async function f3(x, result)
{
    let res = result - 2;
    console.log('f3: ' + res);
    return res;
}

async function f4(x, result)
{
    let res = result +  x;
    console.log('f4: ' + res);
    return res;
}

async function f5(x, result)
{
    let res = result + x ** 3;
    console.log('f5: ' + res);
    return res;
}

async function f6(x, result)
{
    let res = result - 12;
    console.log('f6: ' + res);
    return res;
}

F(3);