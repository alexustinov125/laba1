function func()
{
    let s = document.getElementsByName('text')[0].value
    let array = s.split(/\s/);
    let c = {}
    array.forEach(element => {
        if (c[element])
        {
            c[element] = c[element] + 1
        } 
        else
        {
            c[element] = 1
        }
    });
    array.forEach(element => {
        while(c[element] > 1)
        {
            s = s.replace(element, '');
            c[element]--;
        }
    });
    alert(s);
}
