function greet(lang, color)
{

    let greeting;
    let textColour = color;

    switch (lang) 
    {
        case 'ru':
            greeting = 'Здравствуйте!';
            break;
        case 'en':
            greeting = 'Hello!';
            break;
        case 'fr':
            greeting = 'Bonjour!';
            break;
        case 'de':
            greeting = 'Guten Tag!';
            break;
        default:
            greeting = 'Incorrect language code entered!';
    }

    document.write(`<p style="color:${textColour}">${greeting}</p>`);
}

function func()
{
    let lang = document.getElementsByName('text')[0].value
    let color = document.getElementsByName('text1')[0].value
    greet(lang, color)
}