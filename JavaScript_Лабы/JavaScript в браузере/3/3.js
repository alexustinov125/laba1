function func()
{
    let a = func1(Number(document.getElementsByName('text')[0].value))
    alert(a);
}

function func1(n)
{
    if (n == 1 || n == 0)
    {
        return 1;
    }

    return func1(n - 1) + func1(n - 2);
}