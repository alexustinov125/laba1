function multipyNumeric (obj) 
{
    for (let i in obj)
    {
        if (typeof obj[i] == 'number')
        {
            obj[i] = obj[i] * 2;
        }
    }
}

let myBrowser = {};
myBrowser.name = "Microsoft Internet Explorer";
myBrowser.port = 5;

console.log(myBrowser);

multipyNumeric(myBrowser);

console.log(myBrowser);