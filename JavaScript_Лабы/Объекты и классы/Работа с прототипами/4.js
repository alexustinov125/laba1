// Добавление свойства по умолчанию к встроенному объекту
String.prototype.color = "black";
// добавление добавление свойства sizee к встроенному объекту
String.prototype.size = "Arial 11"
// Добавление (изменение) метода к встроенному объекту
String.prototype.write = stringWrite;

function stringWrite()
{
    console.log("Размер шрифта: " + this.size);
    console.log("Цвет текста: " + this.color);
    console.log("Текст: " + this);
}

// используем измененный класс
let s = new String("Это строка");
s.color = "red";
s.size = "13";
s.write();
let s2 = new String("Вторая строка");
s2.write();
