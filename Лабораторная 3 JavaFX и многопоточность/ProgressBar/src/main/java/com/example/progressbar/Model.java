package com.example.progressbar;

public class Model implements IModel{

    Calc c;

    @Override
    public void some_calc(Updatable updater) {
        if(c != null)
            this.stop();
        c = new Calc(updater);
        c.start();
    }

    public void stop()
    {
        c.CalcStop();
    }

    @Override
    public void pause() {
        c.CalcPause();
    }

    @Override
    public void resume() {
        c.CalcResume();
    }

    @Override
    public boolean is_alive() {
        return c.isAlive();
    }
}

class Calc extends Thread{

    boolean shouldStop;
    boolean shouldPause;
    boolean shouldResume;

    Updatable updater;

    public Calc(Updatable updater)
    {
        this.updater = updater;
    }

    void CalcStop(){
        shouldStop = true;
    }

    void CalcPause(){
        shouldPause = true;
    }

    void CalcResume(){
        shouldResume = true;
    }

    @Override
    public void run() {
        for(int i = 0; i < 1000; i++){
            if(shouldStop)
            {
                break;
            }

            if(shouldPause)
            {
                try {
                    synchronized (System.out)
                    {
                        System.out.wait();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                shouldPause = false;
            }

            updater.update((double) i / 1000);
            try {
                sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
