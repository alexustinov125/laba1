module com.example.onebutton {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.onebutton to javafx.fxml;
    exports com.example.onebutton;
}