class Threat implements Runnable{


    public void run()
    {
        String name = Thread.currentThread().getName();
        while(true){
            synchronized(System.out){
                System.out.notify();
                try {
                    System.out.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(name);
            }
        }
    }

}

public class Main {
    public static void main(String[] args) {
        Threat t1 = new Threat();
        Threat t2 = new Threat();
        new Thread(t1, "t1").start();
        new Thread(t2, "t2").start();

    }
}