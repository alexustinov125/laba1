import java.util.*;

public class Main {
    public static void main(String[] args) {
        List<Product> ProductList = new ArrayList<>();
        Product ball = new Product("Ball", "20.03.2019", 100);
        Product shield = new Product("Shield", "03.11.2020", 100);
        Product sword = new Product("Sword", "20.09.2018", 100);
        Product computer = new Product("Computer", "26.10.2012", 100);
        Product table = new Product("Table", "14.04.2001", 100);

        ProductList.add(ball);
        ProductList.add(shield);
        ProductList.add(sword);
        ProductList.add(computer);
        ProductList.add(table);

        Client c = new Client("Petrov", "Petr", "Petrovich", "28.05.2003", "M");
        c.CreateNewOrder(ProductList);

        List<Employee> e = new ArrayList<>();
        Employee a = new Employee("Petrov", "Petr", "Petrovich", "28.05.2003", "M", "Слесарь", "Старший", 300);
        Employee b = new Employee("Petrov", "Petr", "Petrovich", "28.05.2003", "M", "Слесарь", "Старший", 200);
        a.Recruitment(e, a);
        b.Recruitment(e, b);
        a.Dismissal(e, a);
        Collections.sort(e);
        System.out.println(e);
        System.out.println(MediumSalary(e));
        System.out.println(SummSalary(e));
        System.out.println(Employee.getCounter());
    }
    public static float MediumSalary (List < Employee > e) {
        float s = 0.0F;

        Employee employee;
        for (Iterator var2 = e.iterator(); var2.hasNext(); s += (float) employee.salary) {
            employee = (Employee) var2.next();
        }

        float s1 = 0.0F;
        s1 = s / (float) e.size();
        return s1;
    }

    public static float SummSalary (List < Employee > e) {
        float s = 0.0F;

        Employee employee;
        for (Iterator var2 = e.iterator(); var2.hasNext(); s += (float) employee.salary) {
            employee = (Employee) var2.next();
        }

        return s;
    }
}