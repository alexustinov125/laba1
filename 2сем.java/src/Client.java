import java.util.*;


public class Client extends Human
{
    public List<Order> orderList = new ArrayList<>();

    public void CreateNewOrder(List<Product> lProductList)
    {
        Order o = new Order();
        System.out.println("Список товаров: 1 - Ball; 2 - Shield; 3 - Sword; 4 - Computer; 5 - Table");
        System.out.println("Для того, чтобы закончить создание заказа напишите - exit");
        System.out.println("Выберете товары: ");

        Scanner in = new Scanner(System.in);
        String prod = in.toString();

        while (!prod.equals("exit"))
        {
            prod = in.next();

            if (prod.equals("1"))
            {
                o.AddProduct(lProductList.get(0));
                System.out.println("Товар Ball добавлен в заказ");
            }
            if (prod.equals("2"))
            {
                o.AddProduct(lProductList.get(1));
                System.out.println("Товар Shield добавлен в заказ");
            }
            if (prod.equals("3"))
            {
                o.AddProduct(lProductList.get(2));
                System.out.println("Товар Sword добавлен в заказ");
            }
            if (prod.equals("4"))
            {
                o.AddProduct(lProductList.get(3));
                System.out.println("Товар Computer добавлен в заказ");
            }
            if (prod.equals("5"))
            {
                o.AddProduct(lProductList.get(4));
                System.out.println("Товар Table добавлен в заказ");
            }
        }
        System.out.println(o);
        orderList.add(o);
    }

    public Client(String surname, String name, String patronymic, String data, String pol)
    {
        super(surname, name, patronymic, data, pol);
    }
}
