import java.util.List;

public class Employee extends Human implements Comparable<Employee>
{
    static int counter;
    public String subdivision;
    public String position;
    public int salary;

    public boolean rec;

    @Override
    public int compareTo(Employee e)
    {
        return this.salary - e.salary;
    }

    public String toString()
    {
        return "Employee: " + this.surname + " " + this.name + " " + this.patronymic + " " + this.data + " " + this.pol + " " + this.subdivision + " " + this.position + " " + this.salary;
    }

    public Employee(String surname, String name, String patronymic, String data, String pol, String subdivision, String position, int salary) {
        super(surname, name, patronymic, data, pol);
        ++counter;
        this.subdivision = subdivision;
        this.position = position;
        this.salary = salary;
    }

    public static int getCounter() {
        return counter;
    }

    public void Recruitment(List<Employee> l, Employee e)
    {
        l.add(e);
    }

    public void Dismissal(List<Employee> l, Employee e)
    {
        l.remove(e);
        counter--;
    }
}
