import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Date;

public class Order
{
    static int Order_counter;
    public List<Product> productList = new ArrayList<>();
    public Date data;
    public int price = 0;

    public Order()
    {
        data = new Date();
        Order_counter++;
    }

    void AddProduct(Product thing)
    {
        productList.add(thing);
        Product tProduct = productList.get(productList.size()-1);
        price += tProduct.price;
    }

    public String toString()
    {
        return "Products: " + productList + " Date: " + data + " Price: " + price;
    }
}
