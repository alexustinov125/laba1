import requests

request = "https://static-maps.yandex.ru/1.x/?ll=86.303701,54.422120&size=450,450&z=6&l=map&pl=c:ff0000ff,w:3," \
          "86.086806,55.355177,86.188123,54.674655,87.154984,53.794622,87.945098,52.962816"
response = requests.get(request)

if response:
    with open('./files/8.png', 'wb') as file:
        file.write(response.content)
else:
    print("Ошибка выполнения запроса:")
    print(request)
    print("Http статус:", response.status_code, "(", response.reason, ")")
