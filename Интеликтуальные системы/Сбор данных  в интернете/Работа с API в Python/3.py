import requests

geocoder_request = "https://geocode-maps.yandex.ru/1.x/?format=json&apikey=77ade68f-e067-49cc-803c-e7108c8cdc50" \
                   "&geocode=г.Москва,%20улица%20Красная%20пл-дь,%201"

response = requests.get(geocoder_request)
if response:
    json_response = response.json()

    toponym = json_response["response"]["GeoObjectCollection"]["featureMember"][0]["GeoObject"]

    toponym_address = toponym["metaDataProperty"]["GeocoderMetaData"]["text"]

    toponym_coodrinates = toponym["Point"]["pos"]

    print(toponym_address, "имеет координаты:", toponym_coodrinates)
else:
    print("Ошибка выполнения запроса:")
    print(geocoder_request)
    print("Http статус:", response.status_code, "(", response.reason, ")")
