import requests

request = "https://static-maps.yandex.ru/1.x/?ll=130.706472,-30.739749&size=300,300&z=17&spn=34,35&l=sat"
response = requests.get(request)

if response:
    with open('./files/6.png', 'wb') as file:
        file.write(response.content)
else:
    print("Ошибка выполнения запроса:")
    print(request)
    print("Http статус:", response.status_code, "(", response.reason, ")")
