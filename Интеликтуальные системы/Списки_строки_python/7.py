s = input()
a = s[1::].replace('V', '!V!').split('!')
c = 0
if len(s) == 1 or s[1] == 'V':
    k = 1
else:
    k = 0
for i in a:
    if i == '':
        continue
    elif i[0] == '<':
        c -= len(i)
        print(c * ' ' + s[0] + i.replace('<', s[0]))
        k = 0
    elif i[0] == '>':
        print(c * ' ' + s[0] + i.replace('>', s[0]))
        c += len(i)
        k = 0
    elif i[0] == 'V':
        if k:
            print(c * ' ' + s[0])
        k = 1
if k:
    print(c * ' ' + s[0])
