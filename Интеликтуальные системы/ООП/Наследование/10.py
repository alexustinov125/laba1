class Summator(object):
    def __init__(self, f):
        self.f = f

    def transform(self, x):
        return self.f(x)

    def sum(self, n):
        k = 0
        for i in range(n + 1):
            k += self.transform(i)
        return k


class SquareSummator(Summator):
    def __init__(self):
        super().__init__(lambda x: x ** 2)


class CubeSummator(Summator):
    def __init__(self):
        super().__init__(lambda x: x ** 3)


s = SquareSummator()
print(s.sum(2))

q = CubeSummator()
print(q.sum(2))
