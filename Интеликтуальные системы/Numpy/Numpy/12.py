import numpy as np


table = np.genfromtxt('./files/ABBREV.csv', delimiter=';', dtype=None, names=True, encoding="utf8")

kal = np.sort(table, order='Energ_Kcal', axis=0)
sugar = np.sort(table, order='Sugar_Tot_g', axis=0)
protein = np.sort(table, order='Protein_g', axis=0)
vit_c = np.sort(table, order='Vit_C_mg', axis=0)

print("Самый калорийный: ", kal[-1]["Shrt_Desc"])
print("Самый полезный по содержанию сахара: ", sugar[0]["Shrt_Desc"])
print("Самый протеино-накачанный: ", protein[-1]["Shrt_Desc"])
print("Самый богатый витамином C: ", vit_c[-1]["Shrt_Desc"])
