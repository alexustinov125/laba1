def power(a, n):
    b = 1
    for i in range(abs(n)):
        b *= a
    if n >= 0:
        return b
    else:
        return 1 / b


print(power(2, -2))
