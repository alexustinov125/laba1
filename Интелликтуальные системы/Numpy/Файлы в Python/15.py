with open('./files/15) input.txt', 'r') as file:
    file.seek(0)

    pos = 0
    neg = 0
    null = 0

    for elem in file.read().split():
        if int(elem) > 0:
            pos += 1
        elif int(elem) < 0:
            neg += 1
        else:
            null += 1

    if pos > 0:
        p = f'1 {pos} '
    else:
        p = ''

    if neg > 0:
        n = f'-1 {neg} '
    else:
        n = ''

    if null > 0:
        nul = f'0 {null} '
    else:
        null = ''

    output_file = open('./files/15) output.txt', 'w')
    output_file.write(p + n + nul)
    output_file.close()
