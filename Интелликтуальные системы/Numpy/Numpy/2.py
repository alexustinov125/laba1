import numpy as np


def make_field(size):
    if size % 2 == 0:
        arr = np.ones((size, size), dtype='int8')
        arr[(size + 1) % 2::2, ::2] = 0
        arr[size % 2::2, 1::2] = 0
    else:
        arr = np.zeros((size, size), dtype='int8')
        arr[(size + 1) % 2::2, ::2] = 1
        arr[size % 2::2, 1::2] = 1
    return arr


print(make_field(6))
