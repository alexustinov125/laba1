from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove
from telegram.ext import Application, CommandHandler
import asyncio
from random import randint
TG_TOKEN = "6213381241:AAHNJkFqw3vSVIUOPnzsu2iE1p8QPrMZPOU"

start_panel = [
    ['/dice', '/timer']
]

dice_panel = [
    ['/throw6', '/throw2_6'],
    ['/throw20', '/back']
]

timer_panel = [
    ['/30seconds', '/1minute'],
    ['/5minutes', '/back']
]

close_panel = [
    ['/close']
]

start_markup = ReplyKeyboardMarkup(start_panel, one_time_keyboard=False)
dice_markup = ReplyKeyboardMarkup(dice_panel, one_time_keyboard=False)
time_markup = ReplyKeyboardMarkup(timer_panel, one_time_keyboard=False)
close_markup = ReplyKeyboardMarkup(close_panel, one_time_keyboard=False)


async def start(update, context):
    await update.message.reply_text('Я бот-помощник для настольных игр. Вот, что я могу:',
                                    reply_markup=start_markup)


async def close_keyboard(update, context):
    await update.message.reply_text("OK", reply_markup=ReplyKeyboardRemove())


async def dice(update, context):
    await update.message.reply_text('Вы можете бросить:' + '\n' + 'Один 6-гранный кубик' + '\n' +
                                    'Два 6-гранных кубика' + '\n' + 'Один 20-гранный кубик', reply_markup=dice_markup)


async def throw6(update, context):
    await update.message.reply_text(randint(1, 6))


async def throw2_6(update, context):
    a = randint(1, 6)
    b = randint(1, 6)
    await update.message.reply_text(f'{a}, {b}')


async def throw20(update, context):
    await update.message.reply_text(randint(1, 20))


async def back(update, context):
    await start(update, context)


async def timer(update, context):
    await update.message.reply_text('Вы можете поставить таймер на:' + '\n' + '30 секунд' + '\n' + '1 минуту' + '\n' +
                                    '5 минут', reply_markup=time_markup)


async def set_30_seconds(update, context):
    await update.message.reply_text('Засек 30 секунд', reply_markup=close_markup)
    await asyncio.sleep(30)
    await update.message.reply_text('30 секунд истекло', reply_markup=time_markup)


async def set_1_minute(update, context):
    await update.message.reply_text('Засек 1 минуту', reply_markup=close_markup)
    await asyncio.sleep(60)
    await update.message.reply_text('1 минута истекла', reply_markup=time_markup)


async def set_5_minutes(update, context):
    await update.message.reply_text('Засек 5 минут', reply_markup=close_markup)
    await asyncio.sleep(300)
    await update.message.reply_text('5 минут истекли', reply_markup=time_markup)


def main():
    application = Application.builder().token(TG_TOKEN).build()
    application.add_handler(CommandHandler('start', start))
    application.add_handler(CommandHandler('dice', dice))
    application.add_handler(CommandHandler('throw6', throw6))
    application.add_handler(CommandHandler('throw2_6', throw2_6))
    application.add_handler(CommandHandler('throw20', throw20))
    application.add_handler(CommandHandler('back', back))
    application.add_handler(CommandHandler('timer', timer))
    application.add_handler(CommandHandler('30seconds', set_30_seconds))
    application.add_handler(CommandHandler('1minute', set_1_minute))
    application.add_handler(CommandHandler('5minutes', set_5_minutes))
    application.add_handler(CommandHandler('close', close_keyboard))
    application.run_polling()


if __name__ == '__main__':
    main()
