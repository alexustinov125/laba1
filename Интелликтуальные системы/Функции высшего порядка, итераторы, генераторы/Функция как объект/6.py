import math


def arithmetic_operation(operation):
    if operation == "+":
        return lambda x, y: x + y
    if operation == "*":
        return lambda x, y: x * y
    if operation == "/":
        return lambda x, y: math.floor(x / y) if y != 0 else print("Делить на 0 нельзя")
    if operation == "-":
        return lambda x, y: x - y


operate = arithmetic_operation('*')
print(operate(1, -1))
