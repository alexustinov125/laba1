class Summator(object):
    def transform(self, x):
        return x

    def sum(self, n):
        k = 0
        for i in range(1, n + 1):
            k += self.transform(i)
        return k


class PowerSummator(Summator):
    def __init__(self, b):
        self.b = b

    def transform(self, x):
        return x ** self.b


class SquareSummator(PowerSummator):
    def __init__(self):
        super().__init__(2)


class CubeSummator(PowerSummator):
    def __init__(self):
        super().__init__(3)


c = CubeSummator()
print(c.sum(5))

q = SquareSummator()
print(q.sum(2))

