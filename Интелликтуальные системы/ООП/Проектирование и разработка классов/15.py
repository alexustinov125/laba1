class Transformation:
    def __init__(self, lst):
        self.lst = lst
        self.com_lst = [method for method in dir(Transformation) if not method.startswith("__") and method != "call"]
        print(f"Available commands: {self.com_lst}")

    def make_negative(self):
        self.lst = [i * (-1) if i > 0 else i for i in self.lst]
        print(self.lst)

    def square(self):
        self.lst = [i ** 2 for i in self.lst]
        print(self.lst)

    def strange_command(self):
        self.lst = [i + 1 if not i % 5 else i for i in self.lst]
        print(self.lst)

    def call(self, name, *args, **kwargs):
        if name in self.com_lst:
            return getattr(self, name)(*args, **kwargs)
        else:
            print("There is no method ", name)


trans = Transformation([int(i) for i in input("Введите список чисел.\n").split()])

com_name = input("Введите название команды.\n")
trans.call(com_name)
