package com.example.flag;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.scene.control.Label;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.RadioButton;
import javafx.scene.layout.*;

public class HelloController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private RadioButton blueColor;

    @FXML
    private RadioButton firstBlock;

    @FXML
    private Pane firstPanel;

    @FXML
    private RadioButton greenColor;

    @FXML
    private HBox hBox;

    @FXML
    private Button paint;

    @FXML
    private RadioButton redColor;

    @FXML
    private RadioButton secondBlock;

    @FXML
    private Pane secondPanel;

    @FXML
    private RadioButton thirdBlock;

    @FXML
    private Pane thirdPanel;

    @FXML
    private Label result1;

    @FXML
    private Label result2;

    @FXML
    private Label result3;

    @FXML
    private RadioButton yellowColor;

    String lbl1;
    String lbl2;
    String lbl3;

    @FXML
    void initialize() {
        ToggleGroup colorGroup = new ToggleGroup();
        blueColor.setToggleGroup(colorGroup);
        yellowColor.setToggleGroup(colorGroup);
        greenColor.setToggleGroup(colorGroup);
        redColor.setToggleGroup(colorGroup);

        ToggleGroup panelGroup = new ToggleGroup();
        firstBlock.setToggleGroup(panelGroup);
        secondBlock.setToggleGroup(panelGroup);
        thirdBlock.setToggleGroup(panelGroup);
    }

    @FXML
    public void ChooseFirst()
    {
        redColor.setSelected(false);
        greenColor.setSelected(false);
        blueColor.setSelected(false);
        yellowColor.setSelected(false);
    }

    @FXML
    public void ChooseSecond()
    {
        redColor.setSelected(false);
        greenColor.setSelected(false);
        blueColor.setSelected(false);
        yellowColor.setSelected(false);
    }

    @FXML
    public void ChooseThird()
    {
        redColor.setSelected(false);
        greenColor.setSelected(false);
        blueColor.setSelected(false);
        yellowColor.setSelected(false);
    }

    @FXML
    public void ChooseRed() {
        if(firstBlock.isSelected())
        {
            firstPanel.setStyle("-fx-background-color: #FF0000");
            lbl1 = "красный";
        }
        else if (secondBlock.isSelected())
        {
            secondPanel.setStyle("-fx-background-color: #FF0000");
            lbl2 = "красный";
        }
        else if (thirdBlock.isSelected())
        {
            thirdPanel.setStyle("-fx-background-color: #FF0000");
            lbl3 = "красный";
        }
    }

    @FXML
    public void ChooseYellow() {
        if(firstBlock.isSelected())
        {
            firstPanel.setStyle("-fx-background-color: #FFFF00");
            lbl1 = "желтый";
        }
        else if (secondBlock.isSelected())
        {
            secondPanel.setStyle("-fx-background-color: #FFFF00");
            lbl2 = "желтый";
        }
        else if (thirdBlock.isSelected())
        {
            thirdPanel.setStyle("-fx-background-color: #FFFF00");
            lbl3 = "желтый";
        }
    }

    @FXML
    public void ChoseGreen()
    {
        if(firstBlock.isSelected())
        {
            firstPanel.setStyle("-fx-background-color: #008000");
            lbl1 = "зеленый";
        }
        else if (secondBlock.isSelected())
        {
            secondPanel.setStyle("-fx-background-color: #008000");
            lbl2 = "зеленый";
        }
        else if (thirdBlock.isSelected())
        {
            thirdPanel.setStyle("-fx-background-color: #008000");
            lbl3 = "зеленый";
        }
    }

    @FXML
    public void ChooseBlue() {
        if(firstBlock.isSelected())
        {
            firstPanel.setStyle("-fx-background-color: #0000FF");
            lbl1 = "синий";
        }
        else if (secondBlock.isSelected())
        {
            secondPanel.setStyle("-fx-background-color: #0000FF");
            lbl2 = "синий";
        }
        else if (thirdBlock.isSelected())
        {
            thirdPanel.setStyle("-fx-background-color: #0000FF");
            lbl3 = "синий";
        }
    }

    public void Paint()
    {
        result1.setText(lbl1);
        result2.setText(lbl2);
        result3.setText(lbl3);
    }
}
