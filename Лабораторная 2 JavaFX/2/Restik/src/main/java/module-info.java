module com.example.restik {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.restik to javafx.fxml;
    exports com.example.restik;
}