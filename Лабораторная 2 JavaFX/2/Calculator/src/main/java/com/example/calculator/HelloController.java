package com.example.calculator;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class HelloController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button clearButton;

    @FXML
    private Button eighthButton;

    @FXML
    private Button equalButton;

    @FXML
    private Button fifthButton;

    @FXML
    private Button firstButton;

    @FXML
    private Button fourthButton;

    @FXML
    private Button minusButton;

    @FXML
    private Button multiplyButton;

    @FXML
    private Button ninthButton;

    @FXML
    private Button plusButton;

    @FXML
    private Button plusMinusButton;

    @FXML
    private Button pointButton;

    @FXML
    private Button secondButton;

    @FXML
    private Button seventhButton;

    @FXML
    private Button sixthButton;

    @FXML
    private Label textLabel;

    @FXML
    private Button thirdButton;

    @FXML
    private Button zeroButton;

    @FXML
    void initialize() {
    }

    @FXML
    public void AddZero() {
        if(textLabel.getText().equals("0"))
        {
            textLabel.setText(zeroButton.getText());
        } else textLabel.setText(textLabel.getText() + zeroButton.getText());
    }

    @FXML
    public void AddOne() {
        if(textLabel.getText().equals("0"))
        {
            textLabel.setText(firstButton.getText());
        } else textLabel.setText(textLabel.getText() + firstButton.getText());
    }

    @FXML
    public void AddTwo() {
        if(textLabel.getText().equals("0"))
        {
            textLabel.setText(secondButton.getText());
        } else textLabel.setText(textLabel.getText() + secondButton.getText());
    }

    @FXML
    public void AddThree() {
        if(textLabel.getText().equals("0"))
        {
            textLabel.setText(thirdButton.getText());
        } else textLabel.setText(textLabel.getText() + thirdButton.getText());
    }

    @FXML
    public void AddFour() {
        if(textLabel.getText().equals("0"))
        {
            textLabel.setText(fourthButton.getText());
        } else textLabel.setText(textLabel.getText() + fourthButton.getText());
    }

    @FXML
    public void AddFive() {
        if(textLabel.getText().equals("0"))
        {
            textLabel.setText(fifthButton.getText());
        } else textLabel.setText(textLabel.getText() + fifthButton.getText());
    }

    @FXML
    public void AddSix() {
        if(textLabel.getText().equals("0"))
        {
            textLabel.setText(sixthButton.getText());
        } else textLabel.setText(textLabel.getText() + sixthButton.getText());
    }

    @FXML
    public void AddSeven() {
        if(textLabel.getText().equals("0"))
        {
            textLabel.setText(seventhButton.getText());
        } else textLabel.setText(textLabel.getText() + seventhButton.getText());
    }

    @FXML
    public void AddEight() {
        if(textLabel.getText().equals("0"))
        {
            textLabel.setText(eighthButton.getText());
        } else textLabel.setText(textLabel.getText() + eighthButton.getText());
    }

    @FXML
    public void AddNine() {
        if(textLabel.getText().equals("0"))
        {
            textLabel.setText(ninthButton.getText());
        } else textLabel.setText(textLabel.getText() + ninthButton.getText());
    }

    @FXML
    public void PlusMinus() {
        double n = Double.parseDouble(textLabel.getText()) * -1;
        textLabel.setText(Double.toString(n));
    }

    @FXML
    public void Point() {
        textLabel.setText(textLabel.getText() + ".");
    }

    boolean isPlusClick = false;
    boolean isMinusClick = false;
    boolean isMultiplyClick = false;
    boolean isDivClick = false;

    double result;

    @FXML
    public void Equal() {
        double secondNumber = Double.parseDouble(textLabel.getText());
        if(isPlusClick)
        {
            result = firstNumber + secondNumber;
            textLabel.setText(Double.toString(result));
        }
        else if(isMinusClick)
        {
            result = firstNumber - secondNumber;
            textLabel.setText(Double.toString(result));
        }
        else if(isMultiplyClick)
        {
            result = firstNumber * secondNumber;
            textLabel.setText(Double.toString(result));
        }
        else if(isDivClick)
        {
            if(secondNumber == 0)
            {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Ошибка");
                alert.setHeaderText("На ноль делить нельзя");
                alert.showAndWait();
            }
            else
            {
                result = firstNumber / secondNumber;
                textLabel.setText(Double.toString(result));
            }
        }
    }

    double firstNumber = 0;

    @FXML
    public void Plus() {
        isMinusClick = false;
        isMultiplyClick = false;
        isDivClick = false;

        if(!textLabel.getText().equals("0"))
        {
            firstNumber = Double.parseDouble(textLabel.getText());
            textLabel.setText("");
            isPlusClick = true;
        }
    }

    @FXML
    public void Minus() {
        isPlusClick = false;
        isMultiplyClick = false;
        isDivClick = false;

        if(!textLabel.getText().equals("0"))
        {
            firstNumber = Double.parseDouble(textLabel.getText());
            textLabel.setText("");
            isMinusClick = true;
        }
    }

    @FXML
    public void Multiple() {
        isPlusClick = false;
        isMinusClick = false;
        isDivClick = false;

        if(!textLabel.getText().equals("0"))
        {
            firstNumber = Double.parseDouble(textLabel.getText());
            textLabel.setText("");
            isMultiplyClick = true;
        }
    }

    @FXML
    public void Div() {
        isPlusClick = false;
        isMinusClick = false;
        isMultiplyClick = false;

        if(!textLabel.getText().equals("0"))
        {
            firstNumber = Double.parseDouble(textLabel.getText());
            textLabel.setText("");
            isDivClick = true;
        }
    }

    @FXML
    public void Clear() {
        textLabel.setText("0");
    }
}
